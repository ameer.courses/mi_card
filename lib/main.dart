// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Container(
          child: SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircleAvatar(
                  radius: 60,
                  foregroundImage: AssetImage('images/D.png'),
                ),
                SizedBox(height: 20,),
                Text('Ameer Abo AlShar',
                   style: TextStyle(
                      fontSize: 32,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Akronim',
                   ),
                ),
                SizedBox(height: 10,),
                Text('Full Stack Developer',
                   style: TextStyle(
                      fontSize: 24,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Akronim',
                   ),
                ),
                info(Icon(Icons.phone), '0934801138'),
                info(Icon(Icons.email), 'ameer.mamoon@gmail.com'),
              ],
            ),
          ),
          width: double.infinity,
          height: double.infinity,
          decoration:BoxDecoration(
          gradient: LinearGradient(
          colors: [Colors.blue,Colors.deepPurple],
          begin: Alignment.topLeft,
          end: Alignment.bottomRight
        )

          ) ,
        ),
      ),
    );
  }

  Widget info(Widget icon,String text){
    return Container(
      margin: EdgeInsets.only(top: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          icon,
          SizedBox(width: 15,),
          Text(text)
        ],
      ),
      width: 280,
      height: 60,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
      ),
    );
  }

}
